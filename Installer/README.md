Put the project's built binary here to build the installer.
You can download and extract the *installer-bin.zip* file to this folder in order to quickly assemble all of the installer's other dependencies.
