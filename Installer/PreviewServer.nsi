; The name of the installer
Name "Preview Handler Pack for Windows XP"

; The file to write
OutFile "PrevHandlerPack.exe"

; The default installation directory
InstallDir "$PROGRAMFILES\Preview Handler Pack"

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;--------------------------------
;Version Information

  VIProductVersion "1.0.1.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "Preview Handler Pack for Windows XP"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "For MS Office Outlook 2007"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "Gil Azar"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "� Gil Azar"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Preview Handler Pack for Windows XP Setup"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "1.0.5"

;--------------------------------
; Pages

Page directory
Page instfiles
;UninstPage uninstConfirm
;UninstPage instfiles
;--------------------------------
; The stuff to install

Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Put MSDN package and run its install
  File "PreviewHandlers setup.msi"
  ExecWait 'msiexec /i "PreviewHandlers setup.msi" /q ALLUSERS=2 INSTALLDIR="$INSTDIR"' $0
  Delete /REBOOTOK "$INSTDIR\PreviewHandlers setup.msi"

  !if $0 > 0
  	Abort "MSDN PreviewHandlers setup returned $0"
  !else if $0 < 0
  	Abort "MSDN PreviewHandlers setup returned $0"
  !endif

  ; Put Code Preview package and run its install
  File "CodePreviewHandlerSetup.msi"
  ExecWait 'msiexec /i "CodePreviewHandlerSetup.msi" /q ALLUSERS=2 INSTALLDIR="$INSTDIR"' $0
  Delete /REBOOTOK "$INSTDIR\CodePreviewHandlerSetup.msi"
  ; Less important for installation, we can skip error checking
  
  ; Put main executable and register it
  File "PreviewServer.exe"
  ExecWait '"$INSTDIR\PreviewServer.exe" /regserver' $0
  !if $0 > 0
  	Abort "Error registering preview handler. Got return code $0"
  !else if $0 < 0
  	Abort "Error registering preview handler. Got return code $0"
  !endif
  
SectionEnd ; end the section
