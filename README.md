Preview Handlers for Windows XP
===============================
Implementation of Preview Handlers in Office 2007 on Windows XP Service Pack 2 (and above)

Written by Gil Azar

Last Updated 29-Oct-2011

Introduction
------------
As soon as I've installed Microsoft Office 2007, I've noticed this new Preview Handlers feature integrated into Outlook's Reading Pane. So I thought to myself, "hey, that's pretty cool! Let's download some previewers from the internet!". Alas, no Preview Handler that worked under Windows XP could be found! How can that be??

My spirits were down, but I did not lose hope. I started reading here and there, found this [really cool implementation] [StephenPack] of preview handlers (written in C#) for Windows Vista by Stephen Toub. But as cool as that implementation might be, it only worked under Vista (and above).

So I decided to download the Windows SDK and try my luck with this new (incredibly unsupported) feature. After a few nights I have developed the code you can see here, for free, for any use!

A bit more info
---------------
For some file types I wrote the implementation myself (pdf, windows media, web/xml), and for others (zip, msi, cs, js, vb, sql, and more) I created a quite nifty Preview Handler Wrapper, which basically delegates all the interface calls to the already-implemented preview handlers, managed or not (i.e. which only worked on Vista or not).

The biggest technical problem one has to tackle here is creating a COM server for the preview handler objects which works OUT OF PROCESS (i.e. in its own EXE file) instead of IN PROCESS (the ones that were only available (at my frustration point) and implemented in .NET, i.e. "managed code", i.e. "not for WinXP").

The reason Microsoft had the preview handlers work out of process is probably mainly due to security reasons. The problem with Windows XP (sp2) is that it doesn't have this default preview handler surrogate called prevhost.exe (which only comes with Windows Vista), that basically takes in-process implementations (managed/.NET code among them) and runs them out-of-process (which is what I've done here, actually, only much more generic, and, well, better :-P ).

Supported Types
---------------
- **PDF** - This handler uses Adobe Reader's ActiveX control. If you want to use Foxit Reader instead, please use [Tim Heuer's handler] [FoxitHandler].
- **SWF** - This handler uses Adobe Shockwave Flash's ActiveX control.
- **SNP** - This handler uses [Snapshot Viewer for Microsoft Access](http://www.microsoft.com/downloads/details.aspx?FamilyID=b73df33f-6d74-423d-8274-8b7e6313edfb&DisplayLang=en).
- **HTML/HTM/XML/XSD/XSLT/WSDL/MHT/CONFIG/PSQ/XPS** - This handler works about the same but with Internet Explorer's ActiveX control. I probably should have added a few more file types to this handler.
- **ASF/WMV/WMA/AVI/WAV/MPG/MPE/MPEG/MP3/MIDI/AIFF/AU** and more - The same, but with Windows Media Player's ActiveX. I didn't test all of these, but they should work fine.
- **ZIP/GADGET/MSI/RESX/SNK/KEYS/CSV/RESOURCES** - I didn't implement them myself, but only forwarded interface calls to Stephen Toub's managed preview handlers. 
- **CS/VB/SQL/JS** - like the previous group, I didn't implement them, but added them to my nifty wrapper. For these file types I used the implementation that can be found [here] [TimHeuerPack] in Tim Heuer's blog.

Download
--------
The installation package can be downloaded [here](http://www.azarfamily.org/previewhandlersforwindowsxp/PrevHandlerPack.exe). It installs two prerequisite packages from Microsoft (see Manual Installation below for details), Stephen Toub's MSDN Magazine Managed Preview Handler Framework, Tim Heuer's Code Preview Handlers, and then my addition.

###Notes
1. If you want to override PDF handling to Foxit Reader instead of Adobe Reader, you can install [Tim Heuer's handler] [FoxitHandler] after installing this pack.
2. At this point I'd like to suggest another useful tool by Stephen Toub that lets you edit the list of associated preview handlers installed on your system. You can find the utility [here](http://blogs.msdn.com/toub/archive/2006/12/14/preview-handler-association-editor.aspx). 
3. My source code and installer above are free for any use. I only request that if you use them in your own code or product, please give me some credit for it.

Manual Installation
-------------------
1. Install Microsoft Visual J# 2.0 Redistributable Package available [here](http://www.microsoft.com/downloads/details.aspx?FamilyId=E9D87F37-2ADC-4C32-95B3-B5E3A21BAB2C&displaylang=en).
2. Install Tim Heuer's preview handler pack available [here] [TimHeuerPack].
3. Install Stephen Toub's MSDN Magazine Managed Preview Handler Framework available [here] [StephenPack], found inside the downlodable self-extracting package. Note that the package also includes the package's source code.
4. Put *PreviewServer.exe* (found in the source code package available in the above section) in any folder on your hard drive (where you won't delete or move it afterwards).
5. Run the below command to register the preview handlers in the registry. Note that you must register this server only AFTER you've installed the previous handlers, since it overwrites some of their registry values. If you decide to move the file to a different location or reinstall the previous preview handlers, you'll have to register this server again.
```
PreviewServer.exe /regserver
```

Uninstall
---------
Currently there's no automatic uninstaller. To uninstall, you'll have to follow the following steps:

1. Remove *MSDN Magazine Managed Preview Handlers* from the Add/Remove Programs dialog.
2. Remove *Code File Preview Handler* from the Add/Remove Programs dialog.
3. ONLY THEN remove the rest of the handlers by running the below command. If you used the automatic installer, the default installation path (where PreviewServer.exe is) is *%ProgramFiles%\Preview Handler Pack*.
```
PreviewServer.exe /unregserver
```

License
-------
You may do with this code however you see fit, but expect no warranty.

[StephenPack]:http://msdn.microsoft.com/en-us/magazine/cc163487.aspx
[FoxitHandler]:http://timheuer.com/blog/archive/2008/03/28/foxit-preview-handler-for-xp.aspx
[TimHeuerPack]:http://timheuer.com/blog/archive/2006/12/13/13945.aspx
    