// WebPreview.cpp : Implementation of CWebPreview

#include "stdafx.h"
#include "WebPreview.h"
#include <exdisp.h>		// Defines of stuff like IWebBrowser2. This is an include file with Visual C 6 and above

inline int RECTWIDTH(const RECT &rc) 
{ 
    return (rc.right - rc.left); 
}

inline int RECTHEIGHT(const RECT &rc )
{ 
    return (rc.bottom - rc.top); 
}

// CWebPreview
CWebPreview::CWebPreview()
{
	_ASSERTE( AtlAxWinInit() );

	_hwndParent = NULL;
    _hwndPreview = NULL; 
    _pFrame = NULL;
}

CWebPreview::~CWebPreview()
{ 
    if (_hwndPreview != NULL)
        DestroyWindow(_hwndPreview);

    if (_pFrame != NULL)
        _pFrame->Release();
}


//
// IPreviewHandler
// This method gets called when the previewer gets created
//
HRESULT CWebPreview::SetWindow(HWND hwnd, const RECT *prc)
{
	if (hwnd != NULL && prc != NULL)
    {
        _hwndParent = hwnd; // cache the HWND for later use
        _rcParent   = *prc; // cache the RECT for later use

        if (_hwndPreview == NULL)
			return S_OK;

        // Update preview window parent and rect information
        SetParent(_hwndPreview, _hwndParent);
        SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                     RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
    }

    return S_OK;
}

HRESULT CWebPreview::SetFocus()
{
	if (_hwndPreview==NULL)
		return S_FALSE;
	
	::SetFocus(_hwndPreview);
    return S_OK;
}

HRESULT CWebPreview::QueryFocus(HWND *phwnd)
{
    if (phwnd == NULL)
		return E_INVALIDARG;
    
	*phwnd = ::GetFocus();
        
	if (*phwnd != NULL)
		return S_OK;
	else
		return HRESULT_FROM_WIN32(GetLastError());
}

HRESULT CWebPreview::TranslateAccelerator(MSG *pmsg)
{
	if (_pFrame == NULL)
		return S_FALSE;

    // If your previewer has multiple tab stops, you will need to do appropriate first/last child checking.
    // This particular sample previewer has no tabstops, so we want to just forward this message out 
	return _pFrame->TranslateAccelerator(pmsg);
}

//
// This method gets called when the size of the previewer window changes (user resizes the Reading Pane)
//
HRESULT CWebPreview::SetRect(const RECT *prc)
{
	if (prc == NULL)
		return E_INVALIDARG;

    _rcParent = *prc;

	if (_hwndPreview == NULL)
		return S_OK;

    // Preview window is already created, so set its size and position
    SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                 RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	return S_OK;
}

//
// The main method that extracts relevant information from the file and 
// draws content to the previewer window
//
HRESULT CWebPreview::DoPreview()
{
	// cannot call more than once (Unload should be called before another DoPreview)
	if (_hwndPreview != NULL || _sFilePath.IsEmpty())
		return E_FAIL;

///////////////////////////////////////////////////////////////
	CAxWindow wnd;
	_hwndPreview = wnd.Create(_hwndParent, _rcParent, _T("{8856F961-340A-11D0-A96B-00C04FD705A2}"),
				WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	if (_hwndPreview == NULL)
		return E_FAIL;

	CComPtr<IWebBrowser2> spWebBrowser;
	HRESULT hr = wnd.QueryControl(&spWebBrowser);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

	// Supress dialog boxes
	hr = spWebBrowser->put_Silent(VARIANT_TRUE);
	if (FAILED(hr))
	{
		_ASSERT(false);
		//return hr;	// This is not mandatory, no need to quit
	}

	// Navigate to start page
	CComBSTR sURL(_sFilePath);
	hr = spWebBrowser->Navigate(sURL, NULL, NULL, NULL, NULL);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

	return S_OK;
}

//
// This method gets called when a shell item is de-selected in the listview
//
HRESULT CWebPreview::Unload()
{
	if (!_sFilePath.IsEmpty())
	{
		DeleteFile(_sFilePath);
		_sFilePath.Empty();
	}

    if (_hwndPreview != NULL)
    {
        DestroyWindow(_hwndPreview);
        _hwndPreview = NULL;
    }

    return S_OK;
}

//
// IObjectWithSite methods
//
HRESULT CWebPreview::SetSite(IUnknown *pSite) 
{ 
    __super::SetSite(pSite);

    // Clean up old frame
    if (_pFrame != NULL)
    {
        _pFrame->Release();
        _pFrame = NULL;
    }

    // Get the new frame
    if (m_spUnkSite)
        m_spUnkSite->QueryInterface(IID_PPV_ARGS(&_pFrame));

    return S_OK;
}

//
// IOleWindow methods
//
HRESULT CWebPreview::GetWindow(HWND* phwnd) 
{ 
	if (phwnd == NULL)
		return E_INVALIDARG;

	*phwnd = _hwndParent; 
	return S_OK;
}

HRESULT CWebPreview::ContextSensitiveHelp(BOOL) 
{ 
    return E_NOTIMPL; 
}

//
// IInitializeWithFile methods
// This method gets called when an item gets selected in listview
HRESULT CWebPreview::Initialize(LPCWSTR pszFilePath, DWORD)
{
	if (pszFilePath == NULL)
		return E_INVALIDARG;

	// To create a temporary copy of the input file,
	// get temporary files path
	WCHAR sTempPath[MAX_PATH], sTempFileName[MAX_PATH];
	if (!GetTempPath(MAX_PATH, sTempPath))
	{
		_ASSERT(false);
		return E_FAIL;
	}

	// Get previewed file's extension to append to new filename
	LPCWSTR sExtension = pszFilePath;
	LPCWSTR sTemp = pszFilePath;
	while ((sTemp = wcschr(_wcsinc(sExtension), L'.')) != NULL)
		sExtension = sTemp;

	do
	{
		// Get random tempfile name
		if (!GetTempFileName(sTempPath, L"WPH", 0, sTempFileName))
		{
			_ASSERT(false);
			return E_FAIL;
		}

		// Delete auto-generated file, since we're not gonna use it anyway
		DeleteFile(sTempFileName);

		// Append previewed file's extension
		wcscat_s(sTempFileName, MAX_PATH, sExtension);

	} while (!CopyFile(pszFilePath, sTempFileName, TRUE));

	// Remove read-only attribute from temporary file
	DWORD attr = GetFileAttributes(sTempFileName);
	SetFileAttributes(sTempFileName, attr ^ FILE_ATTRIBUTE_READONLY);

	// Keep the temporary file's name so we could remove it later
	_sFilePath = sTempFileName;
	
	return S_OK;
}
