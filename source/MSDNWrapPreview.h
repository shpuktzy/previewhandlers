// MSDNWrapPreview.h : Declaration of the CMSDNWrapPreview

#pragma once
#include "resource.h"       // main symbols
#include "PreviewServer.h"

#import "MsdnMagPreviewHandlers.tlb" no_namespace

class ATL_NO_VTABLE CMSDNWrapPreview :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CMSDNWrapPreview, &CLSID_MSDNWrapPreview>,
	public IObjectWithSiteImpl<CMSDNWrapPreview>,
	public IPreviewHandler,
    public IOleWindow,
    public IInitializeWithFile
{
public:
DECLARE_REGISTRY_RESOURCEID(IDR_ZIPPREVIEW)
DECLARE_NOT_AGGREGATABLE(CMSDNWrapPreview)

BEGIN_COM_MAP(CMSDNWrapPreview)
	COM_INTERFACE_ENTRY(IPreviewHandler)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY(IInitializeWithFile)
END_COM_MAP()

	CMSDNWrapPreview() {}

// IObjectWithSite override
    IFACEMETHODIMP SetSite(IUnknown *punkSite);

    // IPreviewHandler
	IFACEMETHODIMP SetWindow(HWND hwnd, const RECT *prc);
    IFACEMETHODIMP SetFocus();
    IFACEMETHODIMP QueryFocus(HWND *phwnd);
    IFACEMETHODIMP TranslateAccelerator(MSG *pmsg);
    IFACEMETHODIMP SetRect(const RECT *prc);
    IFACEMETHODIMP DoPreview();
    IFACEMETHODIMP Unload();

    // IOleWindow
    IFACEMETHODIMP GetWindow(HWND *phwnd);
    IFACEMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
  
    // IInitializeWithFile
    IFACEMETHODIMP Initialize(LPCWSTR pszFilePath, DWORD grfMode);

private:
	CComPtr<IPreviewHandler>	_spWrapHandler;
};

OBJECT_ENTRY_AUTO(__uuidof(MSDNWrapPreview), CMSDNWrapPreview)
