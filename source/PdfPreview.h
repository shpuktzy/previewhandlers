// PdfPreview.h : Declaration of the CPdfPreview

#pragma once
#include "resource.h"       // main symbols
#include "PreviewServer.h"


class ATL_NO_VTABLE CPdfPreview :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CPdfPreview, &CLSID_PdfPreview>,
	public IObjectWithSiteImpl<CPdfPreview>,
	public IPreviewHandler,
    public IOleWindow,
    public IInitializeWithFile
{
public:
	CPdfPreview();
	~CPdfPreview();

DECLARE_REGISTRY_RESOURCEID(IDR_PDFPREVIEW)
DECLARE_NOT_AGGREGATABLE(CPdfPreview)

BEGIN_COM_MAP(CPdfPreview)
	COM_INTERFACE_ENTRY(IPreviewHandler)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY(IInitializeWithFile)
END_COM_MAP()

    // IObjectWithSite override
    IFACEMETHODIMP SetSite(IUnknown *punkSite);

    // IPreviewHandler
    IFACEMETHODIMP SetWindow(HWND hwnd, const RECT *prc);
    IFACEMETHODIMP SetFocus();
    IFACEMETHODIMP QueryFocus(HWND *phwnd);
    IFACEMETHODIMP TranslateAccelerator(MSG *pmsg);
    IFACEMETHODIMP SetRect(const RECT *prc);
    IFACEMETHODIMP DoPreview();
    IFACEMETHODIMP Unload();

    // IOleWindow
    IFACEMETHODIMP GetWindow(HWND *phwnd);
    IFACEMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
  
    // IInitializeWithFile
    IFACEMETHODIMP Initialize(LPCWSTR pszFilePath, DWORD grfMode);

private:
    HWND                    _hwndParent;        // parent window that hosts the previewer window; do NOT DestroyWindow this
    RECT                    _rcParent;          // bounding rect of the parent window
    HWND                    _hwndPreview;       // the actual previewer window
    IPreviewHandlerFrame    *_pFrame;           // the previewer frame
//    IStream                 *_pStream;          // the stream for the file we are previewing
	CAtlString				_sFilePath;			// the path of the file we are previewing (temp. copy)
};

OBJECT_ENTRY_AUTO(__uuidof(PdfPreview), CPdfPreview)
