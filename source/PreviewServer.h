

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0595 */
/* at Wed May 15 18:19:42 2013
 */
/* Compiler settings for PreviewServer.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0595 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __PreviewServer_h__
#define __PreviewServer_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __WebPreview_FWD_DEFINED__
#define __WebPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class WebPreview WebPreview;
#else
typedef struct WebPreview WebPreview;
#endif /* __cplusplus */

#endif 	/* __WebPreview_FWD_DEFINED__ */


#ifndef __MSDNWrapPreview_FWD_DEFINED__
#define __MSDNWrapPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class MSDNWrapPreview MSDNWrapPreview;
#else
typedef struct MSDNWrapPreview MSDNWrapPreview;
#endif /* __cplusplus */

#endif 	/* __MSDNWrapPreview_FWD_DEFINED__ */


#ifndef __PdfPreview_FWD_DEFINED__
#define __PdfPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class PdfPreview PdfPreview;
#else
typedef struct PdfPreview PdfPreview;
#endif /* __cplusplus */

#endif 	/* __PdfPreview_FWD_DEFINED__ */


#ifndef __WinMediaPreview_FWD_DEFINED__
#define __WinMediaPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class WinMediaPreview WinMediaPreview;
#else
typedef struct WinMediaPreview WinMediaPreview;
#endif /* __cplusplus */

#endif 	/* __WinMediaPreview_FWD_DEFINED__ */


#ifndef __SwfPreview_FWD_DEFINED__
#define __SwfPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class SwfPreview SwfPreview;
#else
typedef struct SwfPreview SwfPreview;
#endif /* __cplusplus */

#endif 	/* __SwfPreview_FWD_DEFINED__ */


#ifndef __QTPreview_FWD_DEFINED__
#define __QTPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class QTPreview QTPreview;
#else
typedef struct QTPreview QTPreview;
#endif /* __cplusplus */

#endif 	/* __QTPreview_FWD_DEFINED__ */


#ifndef __SnapshotPreview_FWD_DEFINED__
#define __SnapshotPreview_FWD_DEFINED__

#ifdef __cplusplus
typedef class SnapshotPreview SnapshotPreview;
#else
typedef struct SnapshotPreview SnapshotPreview;
#endif /* __cplusplus */

#endif 	/* __SnapshotPreview_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "shobjidl.h"

#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __PreviewServerLib_LIBRARY_DEFINED__
#define __PreviewServerLib_LIBRARY_DEFINED__

/* library PreviewServerLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_PreviewServerLib;

EXTERN_C const CLSID CLSID_WebPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("D99D682B-76E3-471E-8F43-AFC1EC720414")
WebPreview;
#endif

EXTERN_C const CLSID CLSID_MSDNWrapPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("8606C50A-7BE0-4CB5-8DA4-B88E65508196")
MSDNWrapPreview;
#endif

EXTERN_C const CLSID CLSID_PdfPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("F7C1B3AB-83BA-4C78-95A0-CF3F79C0493B")
PdfPreview;
#endif

EXTERN_C const CLSID CLSID_WinMediaPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("47AC3C2F-7033-4D47-AE81-9C94E566C4CC")
WinMediaPreview;
#endif

EXTERN_C const CLSID CLSID_SwfPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("B480AE4C-7B2F-4639-8ABF-6381B333E2C6")
SwfPreview;
#endif

EXTERN_C const CLSID CLSID_QTPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("5604E4AE-B588-47EB-9896-ACD452517858")
QTPreview;
#endif

EXTERN_C const CLSID CLSID_SnapshotPreview;

#ifdef __cplusplus

class DECLSPEC_UUID("72462ACE-79E1-4673-8EC4-5AC3C5BC9AB7")
SnapshotPreview;
#endif
#endif /* __PreviewServerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


