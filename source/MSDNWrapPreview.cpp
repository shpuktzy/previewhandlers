// MSDNWrapPreview.cpp : Implementation of CMSDNWrapPreview

#include "stdafx.h"
#include "MSDNWrapPreview.h"

//
// IPreviewHandler
//
HRESULT CMSDNWrapPreview::SetWindow(HWND hwnd, const RECT *prc)
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->SetWindow(hwnd, prc);
}

HRESULT CMSDNWrapPreview::SetFocus()
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->SetFocus();
}

HRESULT CMSDNWrapPreview::QueryFocus(HWND *phwnd)
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->QueryFocus(phwnd);
}

HRESULT CMSDNWrapPreview::TranslateAccelerator(MSG *pmsg)
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->TranslateAcceleratorW(pmsg);
}

HRESULT CMSDNWrapPreview::SetRect(const RECT *prc)
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->SetRect(prc);
}

HRESULT CMSDNWrapPreview::DoPreview()
{
	if (!_spWrapHandler)
	{
		_ASSERTE(false && "Not initialized yet!");
		return E_FAIL;
	}

	return _spWrapHandler->DoPreview();
}

HRESULT CMSDNWrapPreview::Unload()
{
	if (!_spWrapHandler)
		return S_OK;

	return _spWrapHandler->Unload();
}

//
// IObjectWithSite methods
//
HRESULT CMSDNWrapPreview::SetSite(IUnknown *pSite) 
{ 
    __super::SetSite(pSite);

	CComQIPtr<IObjectWithSite> spObjWithSite(_spWrapHandler);
	if (spObjWithSite)
		return spObjWithSite->SetSite(pSite);

	_ASSERT(false);
	return S_OK;	//E_NOINTERFACE
}

//
// IOleWindow methods
//
HRESULT CMSDNWrapPreview::GetWindow(HWND* phwnd) 
{ 
	CComQIPtr<IOleWindow> spOleWindow(_spWrapHandler);
	if (spOleWindow)
		return spOleWindow->GetWindow(phwnd);

	_ASSERT(false);
	return E_NOINTERFACE;
}

HRESULT CMSDNWrapPreview::ContextSensitiveHelp(BOOL fEnterMode) 
{ 
	CComQIPtr<IOleWindow> spOleWindow(_spWrapHandler);
	if (spOleWindow)
		return spOleWindow->ContextSensitiveHelp(fEnterMode);

	_ASSERT(false);
	return E_NOINTERFACE;
}

//
// IInitializeWithFile methods
// This method gets called when an item gets selected in listview
HRESULT CMSDNWrapPreview::Initialize(LPCWSTR pszFilePath, DWORD grfMode)
{
	CAtlString Filename(pszFilePath);
	int iDotPlace = Filename.ReverseFind(L'.');
	if (iDotPlace<0)
		return E_FAIL;

	HRESULT hr;
	IID clsid = IID_NULL;
	CAtlString Extension = Filename.Right(Filename.GetLength() - iDotPlace - 1);
	if (!Extension.CompareNoCase(L"zip") || !Extension.CompareNoCase(L"gadget"))
		clsid = __uuidof(ZipPreviewHandler);
	else if (!Extension.CompareNoCase(L"msi"))
		clsid = __uuidof(MsiPreviewHandler);
	else if (!Extension.CompareNoCase(L"resx"))
		clsid = __uuidof(ResxPreviewHandler);
    else if (!Extension.CompareNoCase(L"csv"))
        clsid = __uuidof(CsvPreviewHandler);
	else if (!Extension.CompareNoCase(L"resources"))
		clsid = __uuidof(ResourcesPreviewHandler);
	else if (!Extension.CompareNoCase(L"snk") || !Extension.CompareNoCase(L"keys"))
		clsid = __uuidof(SnkPreviewHandler);
	else if (!Extension.CompareNoCase(L"cs") || !Extension.CompareNoCase(L"vb") ||
			 !Extension.CompareNoCase(L"sql") || !Extension.CompareNoCase(L"js"))
	{
		hr = CLSIDFromString(L"{0E1B4233-AEB5-4c5b-BF31-21766492B301}", &clsid);
		if (FAILED(hr))
			return E_FAIL;
	}
	else
		return E_NOTIMPL;

	_spWrapHandler.Release();
	hr = ::CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER, IID_IPreviewHandler, (void**) &_spWrapHandler);
	_ASSERTE(SUCCEEDED(hr) && _spWrapHandler);

	// Set site on created handler
	CComQIPtr<IObjectWithSite> spObjWithSite(_spWrapHandler);
	if (!spObjWithSite)
	{
		_ASSERT(false);
		return E_NOINTERFACE;
	}

	hr = spObjWithSite->SetSite(m_spUnkSite);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

	// Initialize remote preview handler
	CComQIPtr<IInitializeWithFile> spInitWithFile(_spWrapHandler);
	if (spInitWithFile)
	{
		hr = spInitWithFile->Initialize(pszFilePath, grfMode);
		if (SUCCEEDED(hr))
			return hr;
	}

	// Failed to create with a file, let's try with a stream
	CComQIPtr<IInitializeWithStream> spInitWithStream(_spWrapHandler);
	if (spInitWithStream)
	{
		IStream* pStream = NULL;
		hr = SHCreateStreamOnFileEx(pszFilePath, STGM_READ | STGM_FAILIFTHERE, FILE_ATTRIBUTE_READONLY, FALSE, NULL, &pStream);
		if (FAILED(hr) || !pStream)
		{
			_ASSERT(false);
			return hr;
		}

		return spInitWithStream->Initialize(pStream, STGM_READ);
	}

	_ASSERT(false);
	return E_NOINTERFACE;
}
