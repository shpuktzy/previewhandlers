// QTPreview.cpp : Implementation of CQTPreview

#include "stdafx.h"
#include "QTPreview.h"
#import "C:\\Program Files (x86)\\QuickTime\\QTPlugin.ocx" no_namespace raw_interfaces_only


inline int RECTWIDTH(const RECT &rc) 
{ 
    return (rc.right - rc.left); 
}

inline int RECTHEIGHT(const RECT &rc )
{ 
    return (rc.bottom - rc.top); 
}

// CQTPreview
CQTPreview::CQTPreview()
{
	_ASSERTE( AtlAxWinInit() );

	_hwndParent = NULL;
    _hwndPreview = NULL; 
    _pFrame = NULL;
}

CQTPreview::~CQTPreview()
{ 
    if (_hwndPreview != NULL)
        DestroyWindow(_hwndPreview);

    if (_pFrame != NULL)
        _pFrame->Release();
}


//
// IPreviewHandler
// This method gets called when the previewer gets created
//
HRESULT CQTPreview::SetWindow(HWND hwnd, const RECT *prc)
{
	if (hwnd != NULL && prc != NULL)
    {
        _hwndParent = hwnd; // cache the HWND for later use
        _rcParent   = *prc; // cache the RECT for later use

        if (_hwndPreview == NULL)
			return S_OK;

        // Update preview window parent and rect information
        SetParent(_hwndPreview, _hwndParent);
        SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                     RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
    }

    return S_OK;
}

HRESULT CQTPreview::SetFocus()
{
	if (_hwndPreview==NULL)
		return S_FALSE;
	
	::SetFocus(_hwndPreview);
    return S_OK;
}

HRESULT CQTPreview::QueryFocus(HWND *phwnd)
{
    if (phwnd == NULL)
		return E_INVALIDARG;
    
	*phwnd = ::GetFocus();
        
	if (*phwnd != NULL)
		return S_OK;
	else
		return HRESULT_FROM_WIN32(GetLastError());
}

HRESULT CQTPreview::TranslateAccelerator(MSG *pmsg)
{
	if (_pFrame == NULL)
		return S_FALSE;

    // If your previewer has multiple tab stops, you will need to do appropriate first/last child checking.
    // This particular sample previewer has no tabstops, so we want to just forward this message out 
	return _pFrame->TranslateAccelerator(pmsg);
}

//
// This method gets called when the size of the previewer window changes (user resizes the Reading Pane)
//
HRESULT CQTPreview::SetRect(const RECT *prc)
{
	if (prc == NULL)
		return E_INVALIDARG;

    _rcParent = *prc;

	if (_hwndPreview == NULL)
		return S_OK;

    // Preview window is already created, so set its size and position
    SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                 RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	return S_OK;
}

//
// The main method that extracts relevant information from the file and 
// draws content to the previewer window
//
HRESULT CQTPreview::DoPreview()
{
	// cannot call more than once (Unload should be called before another DoPreview)
	if (_hwndPreview != NULL || _bsFilePath.length() == 0)
		return E_FAIL;

	CAxWindow wnd;
	_hwndPreview = wnd.Create(_hwndParent, _rcParent, _T("{4063BE15-3B08-470D-A0D5-B37161CFFD69}"),
				WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	if (_hwndPreview == NULL)
	{
		_ASSERT(false);
		return E_FAIL;
	}

	CComPtr<IQTPluginControl> spControl;
	HRESULT hr = wnd.QueryControl(&spControl);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

    hr = spControl->SetURL(_bsFilePath);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

    hr = spControl->Play();
	if (FAILED(hr))
	{
		_ASSERT(false);
		//return hr;
	}

	return S_OK;
}

//
// This method gets called when a shell item is de-selected in the listview
//
HRESULT CQTPreview::Unload()
{
	_bsFilePath = L"";

    if (_hwndPreview != NULL)
    {
        DestroyWindow(_hwndPreview);
        _hwndPreview = NULL;
    }

    return S_OK;
}

//
// IObjectWithSite methods
//
HRESULT CQTPreview::SetSite(IUnknown *pSite) 
{ 
    __super::SetSite(pSite);

    // Clean up old frame
    if (_pFrame != NULL)
    {
        _pFrame->Release();
        _pFrame = NULL;
    }

    // Get the new frame
    if (m_spUnkSite)
        m_spUnkSite->QueryInterface(IID_PPV_ARGS(&_pFrame));

    return S_OK;
}

//
// IOleWindow methods
//
HRESULT CQTPreview::GetWindow(HWND* phwnd) 
{ 
	if (phwnd == NULL)
		return E_INVALIDARG;

	*phwnd = _hwndParent; 
	return S_OK;
}

HRESULT CQTPreview::ContextSensitiveHelp(BOOL) 
{ 
    return E_NOTIMPL; 
}

//
// IInitializeWithFile methods
// This method gets called when an item gets selected in listview
HRESULT CQTPreview::Initialize(LPCWSTR pszFilePath, DWORD)
{
	if (pszFilePath == NULL)
		return E_INVALIDARG;

	_bsFilePath = pszFilePath;
	
	return S_OK;
}
