// WebPreview.h : Declaration of the CWebPreview

#pragma once
#include "resource.h"       // main symbols
#include "PreviewServer.h"


class ATL_NO_VTABLE CWebPreview :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CWebPreview, &CLSID_WebPreview>,
	public IObjectWithSiteImpl<CWebPreview>,
	public IPreviewHandler,
    public IOleWindow,
    public IInitializeWithFile
{
public:
	CWebPreview();
	~CWebPreview();

DECLARE_REGISTRY_RESOURCEID(IDR_WEBPREVIEW)
DECLARE_NOT_AGGREGATABLE(CWebPreview)

BEGIN_COM_MAP(CWebPreview)
	COM_INTERFACE_ENTRY(IPreviewHandler)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY(IInitializeWithFile)
END_COM_MAP()

    // IObjectWithSite override
    IFACEMETHODIMP SetSite(IUnknown *punkSite);

    // IPreviewHandler
    IFACEMETHODIMP SetWindow(HWND hwnd, const RECT *prc);
    IFACEMETHODIMP SetFocus();
    IFACEMETHODIMP QueryFocus(HWND *phwnd);
    IFACEMETHODIMP TranslateAccelerator(MSG *pmsg);
    IFACEMETHODIMP SetRect(const RECT *prc);
    IFACEMETHODIMP DoPreview();
    IFACEMETHODIMP Unload();

    // IOleWindow
    IFACEMETHODIMP GetWindow(HWND *phwnd);
    IFACEMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
  
    // IInitializeWithFile
    IFACEMETHODIMP Initialize(LPCWSTR pszFilePath, DWORD grfMode);

private:
    HWND                    _hwndParent;        // parent window that hosts the previewer window; do NOT DestroyWindow this
    RECT                    _rcParent;          // bounding rect of the parent window
    HWND                    _hwndPreview;       // the actual previewer window
    IPreviewHandlerFrame    *_pFrame;           // the previewer frame
	CAtlString				_sFilePath;			// the path of the file we are previewing (temp. copy)
};

OBJECT_ENTRY_AUTO(__uuidof(WebPreview), CWebPreview)
