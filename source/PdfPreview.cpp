// PdfPreview.cpp : Implementation of CPdfPreview

#include "stdafx.h"
#include "PdfPreview.h"

inline int RECTWIDTH(const RECT &rc) 
{ 
    return (rc.right - rc.left); 
}

inline int RECTHEIGHT(const RECT &rc )
{ 
    return (rc.bottom - rc.top); 
}

// CPdfPreview
CPdfPreview::CPdfPreview()
{
	_ASSERTE( AtlAxWinInit() );

	_hwndParent = NULL;
    _hwndPreview = NULL; 
    _pFrame = NULL;
}

CPdfPreview::~CPdfPreview()
{ 
    if (_hwndPreview != NULL)
        DestroyWindow(_hwndPreview);

    if (_pFrame != NULL)
        _pFrame->Release();
}


//
// IPreviewHandler
// This method gets called when the previewer gets created
//
HRESULT CPdfPreview::SetWindow(HWND hwnd, const RECT *prc)
{
	if (hwnd != NULL && prc != NULL)
    {
        _hwndParent = hwnd; // cache the HWND for later use
        _rcParent   = *prc; // cache the RECT for later use

        if (_hwndPreview == NULL)
			return S_OK;

        // Update preview window parent and rect information
        SetParent(_hwndPreview, _hwndParent);
        SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                     RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
    }

    return S_OK;
}

HRESULT CPdfPreview::SetFocus()
{
	if (_hwndPreview==NULL)
		return S_FALSE;
	
	::SetFocus(_hwndPreview);
    return S_OK;
}

HRESULT CPdfPreview::QueryFocus(HWND *phwnd)
{
    if (phwnd == NULL)
		return E_INVALIDARG;
    
	*phwnd = ::GetFocus();
        
	if (*phwnd != NULL)
		return S_OK;
	else
		return HRESULT_FROM_WIN32(GetLastError());
}

HRESULT CPdfPreview::TranslateAccelerator(MSG *pmsg)
{
	if (_pFrame == NULL)
		return S_FALSE;

    // If your previewer has multiple tab stops, you will need to do appropriate first/last child checking.
    // This particular sample previewer has no tabstops, so we want to just forward this message out 
	return _pFrame->TranslateAccelerator(pmsg);
}

//
// This method gets called when the size of the previewer window changes (user resizes the Reading Pane)
//
HRESULT CPdfPreview::SetRect(const RECT *prc)
{
	if (prc == NULL)
		return E_INVALIDARG;

    _rcParent = *prc;

	if (_hwndPreview == NULL)
		return S_OK;

    // Preview window is already created, so set its size and position
    SetWindowPos(_hwndPreview, NULL, _rcParent.left, _rcParent.top, 
                 RECTWIDTH(_rcParent), RECTHEIGHT(_rcParent), SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);

	return S_OK;
}

//
// The main method that extracts relevant information from the file and 
// draws content to the previewer window
//
HRESULT CPdfPreview::DoPreview()
{
	// cannot call more than once (Unload should be called before another DoPreview)
	if (_hwndPreview != NULL || _sFilePath.IsEmpty())
		return E_FAIL;

///////////////////////////////////////////////////////////////
	CAxWindow wnd;
	_hwndPreview = wnd.Create(_hwndParent, _rcParent, _T("{ca8a9780-280d-11cf-a24d-444553540000}"),
				WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, WS_EX_CLIENTEDGE);
	if (_hwndPreview == NULL)
		return E_FAIL;

	CComDispatchDriver spAcroReader;
	HRESULT hr = wnd.QueryControl(IID_IDispatch, (void**) &spAcroReader);
	if (FAILED(hr))
	{
		_ASSERT(false);
		return hr;
	}

	// Tell Acrobat Reader ActiveX control to load our file
	CComVariant vtFilename(_sFilePath);
	return spAcroReader.Invoke1(L"LoadFile", &vtFilename, NULL);
}

//
// This method gets called when a shell item is de-selected in the listview
//
HRESULT CPdfPreview::Unload()
{
	DeleteFile(_sFilePath);
	_sFilePath.Empty();

    if (_hwndPreview != NULL)
    {
        DestroyWindow(_hwndPreview);
        _hwndPreview = NULL;
    }

    return S_OK;
}

//
// IObjectWithSite methods
//
HRESULT CPdfPreview::SetSite(IUnknown *pSite) 
{ 
    __super::SetSite(pSite);

    // Clean up old frame
    if (_pFrame != NULL)
    {
        _pFrame->Release();
        _pFrame = NULL;
    }

    // Get the new frame
    if (m_spUnkSite)
        m_spUnkSite->QueryInterface(IID_PPV_ARGS(&_pFrame));

    return S_OK;
}

//
// IOleWindow methods
//
HRESULT CPdfPreview::GetWindow(HWND* phwnd) 
{ 
	if (phwnd == NULL)
		return E_INVALIDARG;

	*phwnd = _hwndParent; 
	return S_OK;
}

HRESULT CPdfPreview::ContextSensitiveHelp(BOOL) 
{ 
    return E_NOTIMPL; 
}

//
// IInitializeWithFile methods
// This method gets called when an item gets selected in listview
HRESULT CPdfPreview::Initialize(LPCWSTR pszFilePath, DWORD)
{
	if (pszFilePath == NULL)
		return E_INVALIDARG;

	// Create a temporary copy of the input file
	WCHAR sTempPath[MAX_PATH], sTempFileName[MAX_PATH];
	if (!GetTempPath(MAX_PATH, sTempPath))
	{
		_ASSERT(false);
		return E_FAIL;
	}

	if (!GetTempFileName(sTempPath, L"PPH", 0, sTempFileName))
	{
		_ASSERT(false);
		return E_FAIL;
	}

	// Got a valid temporary filename, copy the input file into it
	if (!CopyFile(pszFilePath, sTempFileName, FALSE))
	{
		_ASSERT(false);
		return E_FAIL;
	}

	// Remove read-only attribute from temporary file
	DWORD attr = GetFileAttributes(sTempFileName);
	SetFileAttributes(sTempFileName, attr ^ FILE_ATTRIBUTE_READONLY);

	// Keep the temporary file's name so we could remove it later
	_sFilePath = sTempFileName;
	
	return S_OK;
}

