//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PreviewServer.rc
//
#define IDS_PROJNAME                    100
#define IDR_PREVIEWSERVER               101
#define IDR_WEBPREVIEW                  102
#define IDR_ZIPPREVIEW                  103
#define IDR_PDFPREVIEW                  104
#define IDR_WINMEDIAPREVIEW             105
#define IDR_SWFPREVIEW                  106
#define IDR_SNAPSHOT_PREVIEW			107
#define IDR_QTPREVIEW                   108

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         202
#define _APS_NEXT_SYMED_VALUE           107
#endif
#endif
