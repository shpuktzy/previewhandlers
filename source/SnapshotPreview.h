// SnapshotPreview.h : Declaration of the CSnapshotPreview

#pragma once
#include "resource.h"       // main symbols
#include "PreviewServer.h"
#include <comdef.h>

class ATL_NO_VTABLE CSnapshotPreview :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSnapshotPreview, &CLSID_SnapshotPreview>,
	public IObjectWithSiteImpl<CSnapshotPreview>,
	public IPreviewHandler,
    public IOleWindow,
    public IInitializeWithFile
{
public:
	CSnapshotPreview();
	~CSnapshotPreview();

DECLARE_REGISTRY_RESOURCEID(IDR_SNAPSHOT_PREVIEW)
DECLARE_NOT_AGGREGATABLE(CSnapshotPreview)

BEGIN_COM_MAP(CSnapshotPreview)
	COM_INTERFACE_ENTRY(IPreviewHandler)
	COM_INTERFACE_ENTRY(IObjectWithSite)
	COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY(IInitializeWithFile)
END_COM_MAP()

    // IObjectWithSite override
    IFACEMETHODIMP SetSite(IUnknown *punkSite);

    // IPreviewHandler
    IFACEMETHODIMP SetWindow(HWND hwnd, const RECT *prc);
    IFACEMETHODIMP SetFocus();
    IFACEMETHODIMP QueryFocus(HWND *phwnd);
    IFACEMETHODIMP TranslateAccelerator(MSG *pmsg);
    IFACEMETHODIMP SetRect(const RECT *prc);
    IFACEMETHODIMP DoPreview();
    IFACEMETHODIMP Unload();

    // IOleWindow
    IFACEMETHODIMP GetWindow(HWND *phwnd);
    IFACEMETHODIMP ContextSensitiveHelp(BOOL fEnterMode);
  
    // IInitializeWithFile
    IFACEMETHODIMP Initialize(LPCWSTR pszFilePath, DWORD grfMode);

private:
    HWND                    _hwndParent;        // parent window that hosts the previewer window; do NOT DestroyWindow this
    RECT                    _rcParent;          // bounding rect of the parent window
    HWND                    _hwndPreview;       // the actual previewer window
    IPreviewHandlerFrame    *_pFrame;           // the previewer frame
//    IStream                 *_pStream;          // the stream for the file we are previewing
	bstr_t					_bsFilePath;			// the path of the file we are previewing (temp. copy)
};

OBJECT_ENTRY_AUTO(__uuidof(SnapshotPreview), CSnapshotPreview)
