// PreviewServer.cpp : Implementation of WinMain


#include "stdafx.h"
#include "resource.h"
#include "PreviewServer.h"


class CPreviewServerModule : public CAtlExeModuleT< CPreviewServerModule >
{
public :
	DECLARE_LIBID(LIBID_PreviewServerLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_PREVIEWSERVER, "{2DC0007C-509D-47ED-A27D-8E191E72EC21}")
};

CPreviewServerModule _AtlModule;



//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
                                LPTSTR /*lpCmdLine*/, int nShowCmd)
{
    return _AtlModule.WinMain(nShowCmd);
}

